create database nodos;
use nodos;

CREATE TABLE SUCURSAL
(
id INTEGER AUTO_INCREMENT,
direccion TEXT,
horarioAtencion TEXT,
latitud FLOAT(14,12),
longitud FLOAT(14,12),
PRIMARY KEY (id)
) COMMENT='Tabla de nodos tipo sucursal';


CREATE TABLE PUNTO_DE_RETIRO
(
id INTEGER AUTO_INCREMENT,
capacidad NUMERIC,
latitud FLOAT(14,12),
longitud FLOAT(14,12),
PRIMARY KEY (id)
) COMMENT='Tabla de nodos tipo punto de retiro';



CREATE USER 'nodos'@'localhost' IDENTIFIED BY 'nodos';
GRANT ALL PRIVILEGES ON * . * TO 'nodos'@'localhost';
FLUSH PRIVILEGES;

SET GLOBAL time_zone = '-3:00';


INSERT INTO SUCURSAL(direccion,horarioAtencion,latitud,longitud) VALUES('Dr Ignacio Arieta 3429','Lunes a Viernes de 18hs a 22hs',-34.6811808605443,-58.557237761332374);
INSERT INTO SUCURSAL(direccion,horarioAtencion,latitud,longitud) VALUES('Juan Manuel de Rosas 2099','Lunes a Viernes de 18hs a 22hs',-34.65583822322141,-58.61883013219763);

INSERT INTO PUNTO_DE_RETIRO(capacidad,latitud,longitud)VALUES(200,-34.62889767904243, -58.468035558892794);
INSERT INTO PUNTO_DE_RETIRO(capacidad,latitud,longitud)VALUES(900, -34.67973376672893, -58.56862750854235);