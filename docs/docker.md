##Docker

### Construcción Manual

Se debe ejecutar en el raiz del proyecto el siguiente comando.

## Aplicación.


```console

 $> docker build --tag=nodos:latest

```

### Construcción con maven.

```console

 $> mvn spring-boot:build-image

```


### Ejecución

Para iniciar se debe ejecutar el siguiente comando.

```console

 $> docker run -p8080:8080 nodos:latest

```


## MySQL


```console

 $> docker build -f mysql_Dockerfile 

```
