### Modo de uso de docker-compose

El stack para correr esta aplicación consta de 1 nodo con una base de datos (MySQL) y un nodo de aplicación (SpringBoot)

Para iniciar el stack se debe ejecutar:

```console
 $> sudo docker-compose up --build
```

Para hacer bajar el stack se debe ejecutar:

```console
 $> sudo docker-compose down
```