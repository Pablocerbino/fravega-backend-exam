## Frávega IT

### Software engineer
#### Challenge 1 - 
##### Sucursal CRUDChallenge 1 - 

Para mejorar la experiencia del cliente, la sucursal default que se ofrece para store pickup debe ser la más cercana a su ubicación. 
Para esto, una de las funcionalidades que se necesita es conocer la sucursal más cercana a un punto dado (latitud y longitud).
Diseñar e implementar un servicio que exponga en su API las operaciones CRUD (creación, edicion, borrado y lectura por id) de la entidad Nodo y la consulta del nodo más cercana a un punto dado.
Un Nodo puede ser de dos tipos:

 * Sucursal con campos id, dirección, horario de atención, latitud y longitud.
 * Punto de retiro con campos id, capacidad(numérico), latitud y longitud.
 
Diseñar e implementar (unit tests) al menos UN caso de prueba exitoso y uno no exitoso de dominio para la operación de creación de una sucursal.

Diseñar e implementar al menos UN unit test exitoso del cálculo de distancias.

### Evaluación
 Se tendrá en cuenta:

    * El correcto funcionamiento del servicio 
    * Correcta ejecución de los unit tests (validando que se este asertando algo lógico) 
    * Diseño de lasolución 
    * Arquitectura del servicio (clean architecture) 
    * Diseño de la API 
    * Manejo de errores 
    * Claridad del código (clean coding, uso de logs, etc.)
    * Claridad en la documentación (ver Consideraciones) 
    
Implementación de conceptos/herramientas/lenguajes nuevos para el candidato que no conozca o no haya usado antes

Las sucursales creadas se deben persistir en una base de datos (a elección)

La documentación de compilación y ejecución debe estar en el README del proyecto (dependencias y comandos a ejecutar)

La API debe estar documentada en el README del proyecto.
  * Por endpoint: breve descripción, request, response y un ejemplo

La entrega es el repo de github/gitlab (a elección) que debe ser privado

Los logs deben escribirse en standard output (consola)

### Bonus:
 * Usar Docker para build y run del servicio
 * Usar docker-compose para build y run del stack (servicio y base de datos)
 * API documentada con Swagger
 * Agregar healthchecks contra la base de datos

