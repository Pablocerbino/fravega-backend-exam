## Documento de especificación de API rest de Nodos.

### Endpoints:



* Endpoint: /api/nodos
  * Metodo: GET
  * Parametros: N/A
  * Estado de Respuesta: 200
  * Ejemplo de respuesta:
```json
[  
  {
    "capacidad": 0,
    "direccion": "string",
    "horarioAtencion": "string",
    "id": 0,
    "latitud": 0,
    "longitud": 0,
    "puntoDeRetiro": true,
    "sucursal": true
  }
]
  
```
   * Invocacion
     
   ```console
     $> curl -X GET "http://localhost:8080/nodos/api/nodos" -H  "accept: */*"
   ```



* Endpoint: /api/nodos/{id}
  * Metodo: GET
  * Parametros: 
       * Id de nodo
  * Estado de Respuesta: 200
  * Ejemplo de respuesta:
```json
{
  "capacidad": 0,
  "direccion": "string",
  "horarioAtencion": "string",
  "id": 0,
  "latitud": 0,
  "longitud": 0,
  "puntoDeRetiro": true,
  "sucursal": true
}
  
```
   * Invocacion
     
  ```console
     $> curl -X GET "http://localhost:8080/nodos/api/nodos/1" -H  "accept: */*"
   ```


* Endpoint: /api/nodos/{id}
  * Metodo: PUT
  * Parametros: 
       * Id de nodo
       * Representacion Json de un nodo:
```json
{
  "capacidad": 0,
  "direccion": "string",
  "horarioAtencion": "string",
  "id": 0,
  "latitud": 0,
  "longitud": 0
}
```
  * Estado de Respuesta: 200
  * Ejemplo de respuesta:
```json
{
  "capacidad": 0,
  "direccion": "string",
  "horarioAtencion": "string",
  "id": 0,
  "latitud": 0,
  "longitud": 0,
  "puntoDeRetiro": true,
  "sucursal": true
}
  
```
   * Invocacion
     
  ```console
   curl -X PUT "http://localhost:8080/nodos/api/nodos/1" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"capacidad\": 1,  \"direccion\": null,  \"horarioAtencion\": null,  \"latitud\": 15,  \"longitud\": 22}"
  ```




* Endpoint: /api/nodos/
  * Metodo: POST
  * Parametros: 
      * Id de nodo
      * Representación Json de un nodo
      ```json
      {
        "capacidad": 0,
        "direccion": "string",
        "horarioAtencion": "string",
        "latitud": 0,
        "longitud": 0
      }
     ```
  * Estado de Respuesta: 201
  * Ejemplo de respuesta:
```json
{
  "capacidad": 0,
  "direccion": "string",
  "horarioAtencion": "string",
  "id": 0,
  "latitud": 0,
  "longitud": 0,
  "puntoDeRetiro": true,
  "sucursal": true
}
```
   * Invocacion
      
   ```console
    curl -X POST "http://localhost:8080/nodos/api/nodos" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"capacidad\": 10,  \"direccion\": null,  \"horarioAtencion\":null,  \"latitud\": 1,  \"longitud\": 3}"
   ```


* Endpoint: /api/nodos/mas-cercana
  * Metodo: GET
  * Parametros: 
       * Latitud
       * Longitud
  * Estado de Respuesta: 200
  * Ejemplo de respuesta:
```json
{
  "capacidad": 0,
  "direccion": "string",
  "horarioAtencion": "string",
  "id": 0,
  "latitud": 0,
  "longitud": 0,
  "puntoDeRetiro": true,
  "sucursal": true
}
  
```
   * Invocacion
      
  ```console
    curl -X GET "http://localhost:8080/nodos/api/nodos/mas-cercana?latitud=1&longitud=1" -H  "accept: */*"
  ```
  
  * Endpoint: /api/nodos
    * Metodo: DELETE
    * Parametros: 
         * Id de nodo
    * Estado de Respuesta: 204
    * Invocacion
       
     ```console
      $> curl -X DELETE "http://localhost:8080/nodos/api/nodos/2" -H  "accept: */*"
    ```
