## HealthChecks


Para poder usar el monitoreo del estado de la base de datos, disco y ping 
se debe utilizar el siguiente el siguiente endpoint:

```text
http://localhost:8080/nodos/actuator/health
```

Ejemplo de respuesta:

```json
{
  "status": "UP",
  "components": {
    "db": {
      "status": "UP",
      "details": {
        "database": "H2",
        "validationQuery": "isValid()"
      }
    },
    "diskSpace": {
      "status": "UP",
      "details": {
        "total": 315732336640,
        "free": 87350861824,
        "threshold": 10485760,
        "exists": true
      }
    },
    "ping": {
      "status": "UP"
    }
  }
}
```