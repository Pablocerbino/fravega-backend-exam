package com.pcerbino.test.exceptions;

public class ObjectNotFoundException extends RuntimeException {


    public ObjectNotFoundException() {
        super();
    }

    public ObjectNotFoundException(String message, Throwable thw) {
        super(message, thw);
    }

    public ObjectNotFoundException(String message) {
        super(message);
    }

    public ObjectNotFoundException(Throwable thw) {
        super(thw);
    }


}