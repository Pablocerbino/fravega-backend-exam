package com.pcerbino.test.exceptions;

public class NodeException extends RuntimeException {


    public NodeException() {
        super();
    }

    public NodeException(String message, Throwable thw) {
        super(message, thw);
    }

    public NodeException(String message) {
        super(message);
    }

    public NodeException(Throwable thw) {
        super(thw);
    }
}