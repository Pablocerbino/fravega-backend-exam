package com.pcerbino.test.exceptions;

public class IvalidDTOException extends RuntimeException {

    public IvalidDTOException(String message) {
        super(message);
    }
}

