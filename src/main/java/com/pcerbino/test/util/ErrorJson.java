package com.pcerbino.test.util;

import java.util.Map;

public class ErrorJson {
    public Integer status;
    public String error;
    public String message;
    public String timeStamp;
    public String trace;

    public ErrorJson(int status, Map<String, Object> errorAttributes) {
        this.status = status;
        this.error = (String) errorAttributes.get("error");
        this.message = (String) errorAttributes.get("message");
        this.timeStamp = errorAttributes.get("timestamp").toString();
        this.trace = (String) errorAttributes.get("trace");

        if (this.status == 500) {
            this.message = "Excepcion inesperada: por favor, informe este error al administrador del sistema";
        }
    }

    public String toHtml() {
        String retValue = "<html><body><pre>" +
                "Status: " + this.status + "\n" +
                "Error: " + this.error + "\n" +
                "Message: " + this.message + "\n" +
                "Timestamp: " + this.timeStamp + "\n";

        if (trace != null && !trace.isEmpty()) {
            retValue += "Trace:\n" + this.trace + "\n";
        }

        retValue += "</pre></body></html>";
        return retValue;
    }
}

