package com.pcerbino.test.persister.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Data
@NoArgsConstructor
@Table(name = "SUCURSAL")
public class Sucursal extends Nodo {
    @Column(nullable = false)
    private String direccion;
    @Column(nullable = false)
    private String horarioAtencion;

    public Sucursal(String direccion, String horarioDeAtencion, double latitud, double longitud) {
        super(latitud, longitud);
        this.direccion = direccion;
        this.horarioAtencion = horarioDeAtencion;

    }
}
