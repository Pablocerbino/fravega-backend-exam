package com.pcerbino.test.persister.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@NoArgsConstructor
public abstract class Nodo {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false)
    private double longitud;
    @Column(nullable = false)
    private double latitud;

    transient double distance;

    public Nodo(double latitud, double longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
    }
}
