package com.pcerbino.test.persister.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@Table(name = "PUNTO_DE_RETIRO")
public class PuntoDeRetiro extends Nodo {
    @Column(nullable = false)
    private Long capacidad;

    public PuntoDeRetiro(Long capacidad, double latitud, double longitud) {
        super(latitud, longitud);
        this.capacidad = capacidad;

    }


}
