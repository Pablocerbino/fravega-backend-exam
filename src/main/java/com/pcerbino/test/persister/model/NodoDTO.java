package com.pcerbino.test.persister.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
public class NodoDTO implements Serializable {
    @NotNull
    private double longitud;
    @NotNull
    private double latitud;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long capacidad;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String direccion;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String horarioAtencion;
    private Long id;


    public NodoDTO(Nodo nodo) {
        this.longitud = nodo.getLongitud();
        this.latitud = nodo.getLatitud();
        this.id = nodo.getId();
        if (nodo instanceof Sucursal) {
            this.direccion = ((Sucursal) nodo).getDireccion();
            this.horarioAtencion = ((Sucursal) nodo).getHorarioAtencion();
        } else if (nodo instanceof PuntoDeRetiro) {
            this.capacidad = ((PuntoDeRetiro) nodo).getCapacidad();
        }
    }


    public boolean isSucursal() {
        return ((this.direccion != null && !("").equals(this.direccion)) && (this.horarioAtencion != null && !("").equals(this.horarioAtencion))
                && (this.capacidad == null || this.capacidad == 0L));
    }

    public boolean isPuntoDeRetiro() {
        return ((this.direccion == null || ("").equals(this.direccion)) && (this.horarioAtencion == null || ("").equals(this.horarioAtencion))
                && this.capacidad != null);

    }


    public double calculateDistance(double latitud, double longitud) {
        if (latitud == this.latitud && longitud == this.longitud) {
            return 0;
        }
        double earthRadius = 6371;//en kilómetros
        double dLat = Math.toRadians(latitud - this.latitud);
        double dLng = Math.toRadians(longitud - this.longitud);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(latitud)) * Math.cos(Math.toRadians(this.latitud));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        return earthRadius * va2;
    }

}
