package com.pcerbino.test.persister.repository;

import com.pcerbino.test.persister.model.Nodo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodoRepository extends JpaRepository<Nodo, Long> {
}
