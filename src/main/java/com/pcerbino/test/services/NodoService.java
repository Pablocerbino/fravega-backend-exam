package com.pcerbino.test.services;

import com.pcerbino.test.exceptions.IvalidDTOException;
import com.pcerbino.test.exceptions.ObjectNotFoundException;
import com.pcerbino.test.persister.model.NodoDTO;
import com.pcerbino.test.persister.model.PuntoDeRetiro;
import com.pcerbino.test.persister.model.Sucursal;
import com.pcerbino.test.persister.repository.NodoRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NodoService {

    @Autowired
    private NodoRepository nodoRepository;

    @Autowired
    private ModelMapper modelMapper;


    public List<NodoDTO> getAllNodes() {
        log.debug("Recuperando todos los nodos");
        return nodoRepository.findAll().stream().map(NodoDTO::new).collect(Collectors.toList());
    }


    public NodoDTO getNode(Long id) {
        log.debug("Recuperando nodo {}", id);
        return modelMapper.map(nodoRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(String.format("Sucursal / Punto de Retiro %s inexistente", id))), NodoDTO.class);
    }

    public NodoDTO update(Long id, NodoDTO nodo) {
        log.debug("Actualuzando nodo {}", id);
        if (nodo.isSucursal()) {
            return updateSucursal(id, modelMapper.map(nodo, Sucursal.class));
        } else if (nodo.isPuntoDeRetiro()) {
            return updatePuntoDeRetiro(id, modelMapper.map(nodo, PuntoDeRetiro.class));
        }
        throw new IvalidDTOException("Nodo Invalido no es Sucursal ni Punto de retiro");
    }

    private NodoDTO updatePuntoDeRetiro(Long id, PuntoDeRetiro map) {
        PuntoDeRetiro ptoRetiro = modelMapper.map(getNode(id), PuntoDeRetiro.class);
        ptoRetiro.setCapacidad(map.getCapacidad());
        ptoRetiro.setLatitud(map.getLatitud());
        ptoRetiro.setLongitud(map.getLongitud());
        return modelMapper.map(nodoRepository.save(ptoRetiro), NodoDTO.class);

    }

    private NodoDTO updateSucursal(Long id, Sucursal map) {
        Sucursal sucursal = modelMapper.map(getNode(id), Sucursal.class);
        sucursal.setHorarioAtencion(map.getHorarioAtencion());
        sucursal.setDireccion(map.getDireccion());
        sucursal.setLatitud(map.getLatitud());
        sucursal.setLongitud(map.getLongitud());
        return modelMapper.map(nodoRepository.save(sucursal), NodoDTO.class);

    }

    public NodoDTO create(NodoDTO nodo) {
        log.debug("Creando nodo {}", nodo.toString());
        if (nodo.isSucursal()) {
            return modelMapper.map(nodoRepository.save(modelMapper.map(nodo, Sucursal.class)), NodoDTO.class);
        } else if (nodo.isPuntoDeRetiro()) {
            return modelMapper.map(nodoRepository.save(modelMapper.map(nodo, PuntoDeRetiro.class)), NodoDTO.class);
        }
        throw new IvalidDTOException("Nodo Invalido no es Sucursal ni Punto de retiro");
    }

    public void delete(Long id) {
        log.debug("Eliminando nodo {}", id);
        nodoRepository.deleteById(id);
    }

    public NodoDTO getClosest(double latitud, double longitud) {
        log.debug("Recuperando tienda mas cercana para {} - {}", latitud, longitud);
        return modelMapper.map(getAllNodes().stream()
                .sorted(Comparator.comparing(a -> a.calculateDistance(latitud, longitud)))
                .findFirst().get(), NodoDTO.class);
    }
}
