package com.pcerbino.test.api;

import com.pcerbino.test.persister.model.NodoDTO;
import com.pcerbino.test.services.NodoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path = "/api/nodos")
@Api(tags = "Api de mantenimiento de Nodos")
public class NodoController {

    @Autowired
    NodoService nodoService;


    @ApiOperation(value = "Operación que permite obtener todos los nodos dados de alta")
    @GetMapping
    public ResponseEntity<List<NodoDTO>> getAllNodes() {
        return ResponseEntity.ok(nodoService.getAllNodes());
    }


    @ApiOperation(value = "Operación que permite obtener un nodo por su id.")
    @GetMapping(path = "/{id}")
    public ResponseEntity<NodoDTO> geetNodeById(@PathVariable Long id) {
        return ResponseEntity.ok(nodoService.getNode(id));
    }


    @ApiOperation(value = "Operación que permite modificar un nodo.")
    @PutMapping(path = "/{id}")
    public ResponseEntity<NodoDTO> modifyNode(@RequestBody NodoDTO nodo, @PathVariable Long id) {
        return ResponseEntity.ok(nodoService.update(id, nodo));

    }


    @ApiOperation(value = "Operación que permite dar de alta un nuevo nodo")
    @PostMapping
    public ResponseEntity<NodoDTO> createNode(@RequestBody NodoDTO nodo) {
        return ResponseEntity.status(HttpStatus.CREATED).body(nodoService.create(nodo));

    }

    @ApiOperation(value = "Metodo que nos permite dar de baja un nodo")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteNode(@PathVariable Long id) {
        nodoService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @ApiOperation(value = "Operación que permite obtener la tienda mas cercana a un punto")
    @GetMapping(path = "/mas-cercana")
    public ResponseEntity<NodoDTO> getClosestStore(@RequestParam double latitud, @RequestParam double longitud) {
        return ResponseEntity.ok(nodoService.getClosest(latitud, longitud));
    }

}
