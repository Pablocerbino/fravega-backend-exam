package com.pcerbino.test.api;


import com.pcerbino.test.exceptions.IvalidDTOException;
import com.pcerbino.test.exceptions.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalErrorHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    ResponseEntity handleException(Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    @ResponseBody
    ResponseEntity handleObjectNotFound(Exception e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }


    @ExceptionHandler(IvalidDTOException.class)
    @ResponseBody
    ResponseEntity handleIvalidDTOException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Json Invalido, no es sucursal ni punto de retiro");
    }




}
