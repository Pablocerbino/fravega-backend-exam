package com.pcerbino.test;

import com.pcerbino.test.exceptions.IvalidDTOException;
import com.pcerbino.test.exceptions.ObjectNotFoundException;
import com.pcerbino.test.persister.model.Nodo;
import com.pcerbino.test.persister.model.NodoDTO;
import com.pcerbino.test.persister.model.PuntoDeRetiro;
import com.pcerbino.test.persister.model.Sucursal;
import com.pcerbino.test.persister.repository.NodoRepository;
import com.pcerbino.test.services.NodoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;


@RunWith(SpringRunner.class)
public class NodoServiceTest {

    @TestConfiguration
    static class NodoServiceTestContextConfiguration {
        @Bean
        public NodoService nodoService() {
            return new NodoService();
        }


        @Bean
        public ModelMapper modelMapper() {
            ModelMapper modelMapper = new ModelMapper();
            return modelMapper;
        }
    }

    @Autowired
    private NodoService nodoService;

    @MockBean
    private NodoRepository nodoRepository;

    @Before
    public void setUp() {
        Sucursal sucursalSanjusto =
                new Sucursal("Dr Ignacio Arieta 3429", "Lunes a Viernes de 18hs a 22hs", -34.6811808605443, -58.557237761332374);
        sucursalSanjusto.setId(1L);

        Sucursal sucursalMoron =
                new Sucursal("Juan Manuel de Rosas 2099", "Lunes a Viernes de 18hs a 22hs", -34.65583822322141, -58.61883013219763);
        sucursalMoron.setId(2L);

        PuntoDeRetiro ptoRetiroFlores = new PuntoDeRetiro(200L, -34.62889767904243, -58.468035558892794);
        ptoRetiroFlores.setId(3L);

        PuntoDeRetiro ptoRetiroSanJusto = new PuntoDeRetiro(900L, -34.67973376672893, -58.56862750854235);
        ptoRetiroSanJusto.setId(4L);

        PuntoDeRetiro ptoRetiroSanJustoFail = new PuntoDeRetiro(null, -34.67973376672893, -58.56862750854235);


        List<Nodo> allNodos = Arrays.asList(sucursalSanjusto, sucursalMoron, ptoRetiroFlores, ptoRetiroSanJusto);

        Mockito.when(nodoRepository.findById(sucursalSanjusto.getId())).thenReturn(Optional.of(sucursalSanjusto));
        Mockito.when(nodoRepository.findAll()).thenReturn(allNodos);
        Mockito.when(nodoRepository.findById(-99L)).thenReturn(Optional.empty());
        Mockito.when(nodoRepository.findById(1L)).thenReturn(Optional.empty());
        Mockito.when(nodoRepository.save(sucursalMoron)).thenReturn(sucursalMoron);

    }

    @Test(expected = IvalidDTOException.class)
    public void whenIncoompleteObject_thenFail() {
        NodoDTO nodo = new NodoDTO();
        nodo.setLatitud(39.000001);
        nodo.setLongitud(78.00000001);
        nodoService.create(nodo);
    }


    @Test
    public void whenValidDTO_thenCreateOk() {
        NodoDTO nodo = new NodoDTO();
        nodo.setLongitud(3);
        nodo.setLatitud(2);
        nodo.setCapacidad(200L);
        nodoService.create(nodo);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void whenId_ThenFail() {
        NodoDTO nodo = new NodoDTO();
        nodo.setLongitud(3);
        nodo.setLatitud(2);
        nodo.setCapacidad(200L);
        nodoService.update(1L, nodo);
    }


    @Test
    public void whenInvaldId_thenException() {
        assertThrows(ObjectNotFoundException.class, () -> {
            nodoService.getNode(-99L);
        });
    }


    @Test
    public void whenCalularDistancia_thenSucursalCercanaSanjusto() {
        NodoDTO nodo = nodoService.getClosest(-34.67617516967739, -58.57284494342415);
        assertEquals(-34.67973376672893, nodo.getLatitud());
    }


}
