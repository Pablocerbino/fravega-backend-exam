## Examen Backend Fravega

Proyecto de api para mantenimiento de nodos y calculo de sucursal mas proxima.

## Definición del proyecto

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Enunciado](docs/enunciado.md)

## ¿Como compilar el proyecto?

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Compilar](docs/compilar.md)

## ¿Como construyo el docker?

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Docker](docs/docker.md)

## ¿Como ejecutar docker-compose?

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Docker-compose](docs/docker-compose.md)

## Documentación de api mediante Swagger

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Swagger](docs/swagger.md)

## Documentación de Api

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Documentación Api](docs/api.md)



## HealtChecks para la aplicación.

### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Healthchecks](docs/healthchecks.md)





