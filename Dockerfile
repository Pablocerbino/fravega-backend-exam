FROM adoptopenjdk/openjdk11:alpine
ENV TZ=America/Argentina/Buenos_Aires
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
MAINTAINER pablo.cerbino
COPY target/fravega-backend-exam-1.0.0-SNAPSHOT.jar fravega-backend-exam-1.0.0-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/fravega-backend-exam-1.0.0-SNAPSHOT.jar"]
EXPOSE 8080
